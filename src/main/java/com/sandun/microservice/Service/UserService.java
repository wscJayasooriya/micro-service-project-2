package com.sandun.microservice.Service;

import com.sandun.microservice.Entity.User;
import com.sandun.microservice.Repository.UserRepository;
import com.sandun.microservice.ValueObject.Department;
import com.sandun.microservice.ValueObject.ResponseTemplateVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RestTemplate restTemplate;

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public ResponseTemplateVO getUserWithDepartment(Long userId) {
        ResponseTemplateVO templateVO = new ResponseTemplateVO();
        User user = userRepository.findByUserId(userId);

        Department department = restTemplate.getForObject("http://DEPARTMENT-SERVICES/api/departments/get/" + user.getDepartmentId(),Department.class);

        templateVO.setUser(user);
        templateVO.setDepartment(department);

        return templateVO;
    }
}
